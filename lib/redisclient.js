var redis = require('redis');
var url = require('url');
var redisURL = url.parse(process.env.REDISCLOUD_URL || "http://127.0.0.1:6379");
var redisClient = redis.createClient(redisURL.port, redisURL.hostname, {no_ready_check: true});
var redisAuth = redisURL.auth;
var dbID = process.env.DBID || 1;
if(redisAuth!=null) {
  redisClient.auth(redisAuth.split(":")[1]);
}

redisClient.on('connection', function () {
  console.log('Redis Ready!');
});

redisClient.on('error', function (err) {
  console.log('Error ' + err);
});

redisClient.on('connect', function () {
  console.log('Redis is ready');
});

redisClient.select(dbID);

module.exports = redisClient;