var Promise = require('bluebird'),
    client = require('../lib/redisclient')
    _ = require('underscore');

var Config = function(config) {
  this.askfastAccountId = null;
  this.askfastKey = null;

  _.extend(this, config);
}

Config.load = function() {
  return new Promise(function(resolve, reject){
    client.hgetall("config", function(err, config){
      if(err) {
        return reject(err);
      }
      resolve(new Config(config));
    })
  })
}

Config.prototype.save = function() {
  var config = this;
  return new Promise(function(resolve, reject) {
    client.hmset("config", config, function(err, res){
      if(err) {
        return reject(err);
      }
      var result = false;
      if(res=="OK") {
        result = true;
      }
      resolve(result);
    })
  });
}

module.exports = Config;
