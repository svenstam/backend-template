var Promise = require('bluebird');
var client = require('../lib/redisclient');
var User = require('./user');
var uuid = require('node-uuid');
var _ = require('underscore');

var Group = function(group) {
  this.id = null;
  this.name = null;

  _.extend(this, group);
}

Group.findAll = function() {
  return new Promise(function(resolve, reject) {
    client.hgetall("groups", function (err, groupIds) {
      if (err) {
        reject(err);
        return;
      }
      var groups = [];
      Promise.all(Object.keys(groupIds))
        .each(function(key) { groups.push(new Group({id: key, name: groupIds[key]}))})
        .then(function() {resolve(groups)})
    });
  });
}

Group.getGroup = function(groupId) {
  return new Promise(function (resolve, reject) {
    client.hget("groups", groupId, function (err, name) {
      if (err) {
        reject(err);
        return;
      }

      if(name==null) {
        resolve();
      } else {
        resolve(new Group({id: groupId, name: name}));
      }
    });
  });
}

Group.create = function(name) {
  return new Promise(function(resolve, reject) {
    var id = uuid.v4();

    client.hset("groups", id, name, function (err, res) {
      if (err) {
        reject(err);
        return;
      }

      resolve(new Group({id: id, name: name}));
    });
  });
}

Group.exists = function(id) {
  return new Promise(function(resolve, reject) {
    client.hexists("groups", id, function (err, res) {
      if (err) {
        reject(err);
        return;
      }

      resolve(res);
    });
  });
}

Group.prototype.save = function() {
  var group = this;
  return new Promise(function(resolve, reject) {
    Group.exists(group.id).then(function(exists){

      if(exists) {
        return client.hset("groups", group.id, group.name, function (err, res) {
          if (err) {
            reject(err);
            return;
          }

          resolve(true);
        });
      } else {
        resolve(false);
      }
    },function(err){
      reject(err);
    });
  });
}

Group.prototype.delete = function() {
  var group = this;
  return new Promise(function(resolve, reject) {
    group.removeAllMembers().then(function(){
      client.hdel("groups", group.id, function(err, res){
        if(err) {
          return reject(err);
        }
        if(res==1) {
          resolve(true);
        } else {
          resolve(false);
        }
      })
    })
  });
}

Group.prototype.getMembers = function() {
  var group = this;
  return new Promise(function(resolve, reject) {
    client.smembers("group:"+group.id+":members", function(err, members) {
      if (err) {
        return reject(err);
      }

      resolve(members);
    });
  });
}

Group.prototype.addMember = function(id) {
  var group = this;
  return new Promise(function(resolve, reject) {
    User.getUser(id).then(function(user){
      if(user==null) {
        return resolve(false);
      }

      client.sadd("group:"+group.id+":members", id, function(err, result) {
        if (err) {
          return reject(err);
        }

        var res = false;
        if(result==1) {
          res = true;
        }
        resolve(res);
      });
    });
  });
}

Group.prototype.removeMember = function(id) {
  var group = this;
  return new Promise(function(resolve, reject) {
    User.getUser(id).then(function(user){
      if(user==null) {
        return resolve(false);
      }

      client.srem("group:"+group.id+":members", id, function(err, result) {
        if (err) {
          return reject(err);
        }

        var res = false;
        if(result==1) {
          res = true;
        }
        resolve(res);
      });
    });
  });
}

Group.prototype.removeAllMembers = function() {
  var group = this;
  return new Promise(function(resolve, reject) {
    client.del("group:"+group.id+":members", function(err, res) {
      if(err) {
        return reject(err);
      }
      if(res=="ok") {
        return resolve(true);
      }
      resolve(false);
    });
  });
}

Group.prototype.hasMember = function(id) {
  var group = this;
  return new Promise(function(resolve, reject) {
    client.sismembers("group:"+group.id+":members", id, function(err, result) {
      if (err) {
        return reject(err);
      }

      var res = false;
      if(result==1) {
        res = true;
      }
      resolve(res);
    });
  });
}


module.exports = Group;