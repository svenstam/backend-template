
var role = {
  SU: 0,
  ADMIN: 1,
  USER: 2,
  CLIENT: 3,
  VIEWER: 4
}

module.exports = role;