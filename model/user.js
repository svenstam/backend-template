var Promise = require('bluebird');
var client = require('../lib/redisclient');
var uuid = require('node-uuid');
var _ = require('underscore');
var Role = require('./role');
var State = require('./state');
var Address = require('./address');

var User = function(user) {
  this.username = "";
  this.password = "";
  this.name = "";
  this.role = Role.USER;

  this.state = State.INACTIVE;

  _.extend(this, user);
}

User.findAll = function() {
  return new Promise(function (resolve, reject) {
    client.hgetall("users", function (err, users) {
      if(err) {
        reject(err);
        return;
      }
      resolve(users);
    });
  });
}

User.getUser = function(userId) {
  return new Promise(function (resolve, reject) {
    client.hgetall("user:" + userId,  function (err, user) {
      if(err) {
        reject(err);
        return;
      }
      if(user) {
        resolve(new User(user));
        return;
      }

      resolve();
    });
  });
}

User.getUserId = function(username) {
  return new Promise(function(resolve, reject){
    client.hget("users", username, function (err, userId) {
      if(err) {
        return reject(err);
      }

      resolve(userId);
    });
  });
}

User.exists = function(username) {
  return new Promise(function(resolve, reject) {
    client.hexists("users", username, function (err, exists) {

      if (err) {
        reject(err);
        return;
      }

      resolve(exists);
    });
  });
}

User.create = function(user) {
  return new Promise(function(resolve, reject) {
    if(user.id==null) {
      user.id = uuid.v4();
    }

    User.exists(user.username).then(function(res) {
      if(!res) {
        client.hset("users", user.username, user.id, function (err, res) {
          if (err) {
            reject(err);
            return;
          }

          client.hmset("user:" + user.id,
            user, function (err, res) {
              if (err) {
                reject(err);
                return;
              }

              resolve(user);
            });
        });
      } else {
        resolve();
      }
    });
  });
}

User.prototype.save = function() {
  var user = this;
  return new Promise(function(resolve, reject) {
    client.hexists("users", user.username, function(err, res){

      if(err) {
        reject(err);
        return;
      }

      var result = false;
      if(res) {
        return client.hmset("user:" + user.id,
          user, function (err, res) {
            if (err) {
              reject(err);
              return;
            }

            if(res=="OK") {
              result = true;
            }
            resolve(result);
          });
      }
      resolve(result);
    });
  });
}

User.prototype.delete = function() {
  var user = this;
  return new Promise(function(resolve, reject) {
    client.hexists("users", user.username, function(err, res){

      if(err) {
        reject(err);
        return;
      }
      var result = false;
      if(res) {
        return client.del("user:" + user.id, function (err, res1) {
          if (err) {
            reject(err);
            return;
          }
          if(res1!==1) {
            return resolve(result);
          }

          return user.removeAddresses().then(function(count){

            return client.hdel("users", user.username, function(err, res2){
              if (err) {
                reject(err);
                return;
              }
              if(res2===1) {
                result = true;
              }
              resolve(result);
            });
          });
        });
      }
      resolve(result);
    });
  });
}

User.prototype.addAddress = function(type, value) {
  var user = this;
  return new Promise(function(resolve, reject){
    var address = new Address(type, value);
    client.sadd("addresses:" + value, user.id, function(err, res){
      if(err) {
        return reject(err);
      }

      if(res) {
        return client.hset("user:"+user.id+"address", value, type, function(err, res){
          if(err) {
            return reject(err);
          }

          resolve(true);
        });
      }
    })
  });
}

User.prototype.getAddresses = function() {
  var user = this;
  return new Promise(function(resolve, reject){
    client.hgetall("user:" + user.id + "address", function (err, res) {
      if(err) {
        return reject(err);
      }
      var addresses = [];
      _.each(res, function(val, key){
        addresses.push(new Address(val, key));
      });
      resolve(addresses);
    });
  });
}

User.prototype.removeAddress = function(address) {
  var user = this;
  return new Promise(function(resolve, reject) {
    client.srem("addresses:" + address, user.id, function (err, res1) {
      if(err) {
        return reject(err);
      }

      client.hdel("user:" + user.id + "address", address, function(err, res2){
        if(err) {
          return reject(err);
        }

        resolve(res2);
      })
    });
  });
}

User.prototype.removeAddresses = function() {
  var user = this;
  return new Promise(function(resolve, reject) {
    user.getAddresses().then(function(addresses) {
      var promisses = [];
      _.each(addresses, function(address){
        promisses.push(user.removeAddress(address.value));
      });
      Promise.all(promisses).then(function(res){
        var sum = _.reduce(res, function(memo, num){ return memo + num; }, 0);
        resolve(sum);
      });
    });
  });
}

module.exports = User;