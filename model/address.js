var Promise = require('bluebird');
var client = require('../lib/redisclient');

var Address = function() {
  this.type = "";
  this.value = "";
}

var Address = function(type, value) {
  this.type = type;
  this.value = value;
}

Address.findAddress = function(address) {
  return new Promise(function(resolve, reject){
    client.smembers("addresses:" + address, function(err, res) {
      if(err) {
        return reject(err);
      }

      resolve(res);
    });
  })
}
module.exports = Address;

