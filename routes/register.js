var redis = require("redis");
var Promise = require('bluebird');
var auth = require('./auth');
var client = require('../lib/redisclient').redisClient;
var User = require('../model/user');
var Role = require('../model/role');

var register = {
  register: function(req, res) {
    var username = req.body.username || '';
    var password = req.body.password || '';
    var name = req.body.name || '';

    if (username == '' || password == '') {
      res.status(400);
      res.json({
        "status": 400,
        "message": "A username and password need to be given"
      });
      return;
    }

    User.exists(username).then(function(exists){
      if(exists) {
        res.status(409);
        res.json({
          "status": 409,
          "message": "User with username already exists"
        });
        return;
      }

      var user = new User();
      user.username = username;
      user.password = password;
      user.name = name;
      user.role = Role.USER;

      User.create(user).then(function(newUser) {
        if(newUser) {
          return res.json(newUser);
        }

        res.status(500);
        res.json({
          "status": 500,
          "message": "Something went wrong while creating user"
        });
      }, function(err){
        res.status(500);
        res.json({
          "status": 500,
          "message": "Something went wrong saving the user",
          "error": err
        });
      });
    });
  }
}

module.exports = register;