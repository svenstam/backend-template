var jwt = require('jwt-simple');
var Promise = require('bluebird');
var client = require('../lib/redisclient').redisClient;
var User = require('../model/user');

var auth = {

  login: function(req, res) {

    var username = req.body.username || '';
    var password = req.body.password || '';

    if (username == '' || password == '') {
      res.status(401);
      res.json({
        "status": 401,
        "message": "Invalid credentials"
      });
      return;
    }

    // Fire a query to your DB and check if the credentials are valid
    auth.validate(username, password).then(function(dbUserObj){
      if (!dbUserObj) { // If authentication fails, we send a 401 back
        res.status(401);
        res.json({
          "status": 401,
          "message": "Invalid credentials"
        });
        return;
      }

      if (dbUserObj) {

        // If authentication is success, we will generate a token
        // and dispatch it to the client

        res.json(genToken(dbUserObj));
      }
    });
  },

  validate: function(username, password) {

    return new Promise(function (resolve, reject) {
      User.getUserId(username).then(function(userId) {

        if(userId!=null) {
          User.getUser(userId).then(function(user){
            if(user.password==password) {
              resolve(user);
              return;
            }

            resolve();
          }, function(err){
            reject(err);
          });
        } else {
          resolve();
        }

      },function(err){
        reject(err);
      });
    });
  },

  validateUser: function(userId) {
    return User.getUser(userId);
  }
}

// private method
function genToken(user) {
  var expires = expiresIn(7); // 7 days
  var token = jwt.encode({
    userId: user.id,
    exp: expires
  }, require('../config/secret')());

  return {
    token: token,
    expires: expires,
    user: user
  };
}

function expiresIn(numDays) {
  var dateObj = new Date();
  return dateObj.setDate(dateObj.getDate() + numDays);
}

module.exports = auth;