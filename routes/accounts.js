var User = require("../model/user");

var accounts = {

  getAll: function(req, res) {
    User.findAll().then(function(users){
      res.json(users);
    });
  },

  getOne: function(req, res) {
    var id = req.params.id;
    User.getUser(id).then(function(user){
      res.json(user);
    }, function(err){
      res.status(500);
      res.json({
        "status": 500,
        "message": "Something went wrong while creating user"
      });
    });
  },

  create: function(req, res) {
    var newuser = req.body;
    var errMsg = null;
    if(newuser.username == null) {
      errMsg = "No username given.";
    } else if(newuser.password == null) {
      errMsg = "No password given.";
    } else if(newuser.name == null) {
      errMsg = "No name given.";
    }

    if(errMsg!=null) {
      res.status(400);
      return res.json({
        "status": 400,
        "message": errMsg
      });
    }

    User.create(newuser).then(function(user) {
      if(user) {
        return res.json(user);
      }

      res.status(409);
      res.json({
        "status": 409,
        "message": "Username already exists"
      });
    }, function(err){
      res.status(500);
      res.json({
        "status": 500,
        "message": "Something went wrong saving the user",
        "error": err
      });
    });
  },

  update: function(req, res) {
    var updateuser = req.body;
    var id = req.params.id;
    User.getUser(id).then(function(user) {

      if(updateuser.password!=null) {
        user.password = updateuser.password;
      }

      if(updateuser.name!=null) {
        user.name = updateuser.name;
      }

      if(updateuser.role != null) {
        user.role = updateuser.role;
      }

      if(updateuser.state != null) {
        user.state = updateuser.state;
      }

      user.save().then(function(saved) {
        if(saved) {
          return res.json(user);
        }

        res.status(500);
        res.json({
          "status": 500,
          "message": "Something went wrong saving the user"
        });
      },function(err){
        res.status(500);
        res.json({
          "status": 500,
          "message": "Something went wrong saving the user",
          "error": err
        });
      });

    }, function(err){
      res.status(500);
      res.json({
        "status": 500,
        "message": "Something went wrong saving the user",
        "error": err
      });
    });
  },

  delete: function(req, res) {
    var id = req.params.id;
    User.getUser(id).then(function(user) {

      if(user==null) {
        res.status(400);
        return res.json({
          "status": 400,
          "message": "Userid not found"
        });
      }

      user.delete().then(function(deleted){
        if(deleted) {
         return res.json(true);
        }

        res.status(500);
        res.json({
          "status": 500,
          "message": "Something went wrong deleting the user"
        });
      });
    }, function(err){
      res.status(500);
      res.json({
        "status": 500,
        "message": "Something went wrong deleting the user",
        "err": err
      });
    });
  }
};

module.exports = accounts;