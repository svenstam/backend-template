var express = require('express');
var router = express.Router();

var auth = require('./auth');
var register = require('./register');
var accounts = require('./accounts');
var groups = require('./groups');

/*
 * Routes that can be accessed by any one
 */
router.post('/login', auth.login);
router.post('/register', register.register);

/*
 * Account routes
 */
router.get('/api/accounts', accounts.getAll);
router.post('/api/accounts', accounts.create);
router.get('/api/accounts/:id', accounts.getOne);
router.put('/api/accounts/:id', accounts.update);
router.delete('/api/accounts/:id', accounts.delete);

router.get('/api/groups', groups.getAll);
router.post('/api/groups', groups.create);
router.get('/api/groups/:groupid', groups.getOne);
router.put('/api/groups/:groupid', groups.update);
router.delete('/api/groups/:groupid', groups.delete);

router.get('/api/groups/:groupid/members', groups.getMembers);
router.post('/api/groups/:groupid/members/:memberid', groups.addMember);
router.delete('/api/groups/:groupid/members/:memberid', groups.removeMember);

module.exports = router;