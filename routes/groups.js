var Group = require('../model/group');

var groups = {

  getAll: function (req, res) {
    Group.findAll().then(function (groups) {
      res.json(groups);
    });
  },

  getOne: function(req, res) {
    var id = req.params.groupid;
    Group.getGroup(id).then(function(group){
      res.json(group);
    }, function(err){
      res.status(500);
      res.json({
        "status": 500,
        "message": "Something went wrong loading the group"
      });
    });
  },

  create: function(req, res) {
    var newGroup = req.body;
    var errMsg = null;
    if(newGroup.name == null) {
      errMsg = "No name given.";
    }

    if(errMsg!=null) {
      res.status(400);
      return res.json({
        "status": 400,
        "message": errMsg
      });
    }

    Group.create(newGroup.name).then(function(group) {
      if(group) {
        return res.json(group);
      }

      res.status(409);
      res.json({
        "status": 409,
        "message": "Group already exists"
      });
    }, function(err){
      res.status(500);
      res.json({
        "status": 500,
        "message": "Something went wrong creating the group",
        "error": err
      });
    });
  },

  update: function(req, res) {
    var updatedgroup = req.body;
    var id = req.params.groupid;
    Group.getGroup(id).then(function(group) {

      if(updatedgroup.name!=null) {
        group.name = updatedgroup.name;
      }

      group.save().then(function(saved) {
        if(saved) {
          return res.json(group);
        }

        res.status(500);
        res.json({
          "status": 500,
          "message": "Something went wrong updating the group"
        });
      },function(err){
        res.status(500);
        res.json({
          "status": 500,
          "message": "Something went wrong updating the group",
          "error": err
        });
      });

    }, function(err){
      res.status(500);
      res.json({
        "status": 500,
        "message": "Something went wrong updated the group",
        "error": err
      });
    });
  },

  delete: function(req, res) {
    var id = req.params.groupid;
    Group.getGroup(id).then(function(group) {

      if(group==null) {
        res.status(400);
        return res.json({
          "status": 400,
          "message": "GroupId not found"
        });
      }

      group.delete().then(function(deleted){
        if(deleted) {
          return res.json(true);
        }

        res.status(500);
        res.json({
          "status": 500,
          "message": "Something went wrong deleting the group"
        });
      });
    }, function(err){
      res.status(500);
      res.json({
        "status": 500,
        "message": "Something went wrong deleting the group",
        "err": err
      });
    });
  },

  getMembers: function(req, res) {
    var id = req.params.groupid;
    Group.getGroup(id).then(function(group) {
      if(group==null) {
        res.status(400);
        return res.json({
          "status": 400,
          "message": "GroupId not found"
        });
      }
      group.getMembers().then(function(members){

        res.json(members);
      });
    });
  },

  addMember: function(req,res) {
    var id = req.params.groupid;
    var memberId = req.params.memberid;
    Group.getGroup(id).then(function(group) {
      if(group==null) {
        res.status(400);
        return res.json({
          "status": 400,
          "message": "GroupId not found"
        });
      }
      group.addMember(memberId).then(function(result) {
        if(result==false) {
          res.status(400);
          return res.json({
            "status": 400,
            "message": "MemberId not found"
          });
        }
        res.json(result);
      });
    });
  },

  removeMember: function(req, res) {
    var id = req.params.groupid;
    var memberId = req.params.memberid;
    Group.getGroup(id).then(function(group) {
      if(group==null) {
        res.status(400);
        return res.json({
          "status": 400,
          "message": "GroupId not found"
        });
      }
      group.removeMember(memberId).then(function(result) {
        if(result==false) {
          return res.json({
            "status": 400,
            "message": "MemberId not found"
          });
        }
        res.json(result);
      });
    });
  }

}

module.exports = groups;