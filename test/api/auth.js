var chai = require("chai"),
  assert = chai.assert,
  request = require('supertest'),
  client = require('../../lib/redisclient'),
  User = require('../../model/user.js'),
  Role = require('../../model/role.js'),
  app = require('../../server.js');

describe('POST /login', function () {

  var testuser = new User();
  testuser.username = "testuser1";
  testuser.password = "password";
  testuser.name = "Test User";
  testuser.role = Role.USER;

  var userId = null;

  beforeEach(function (done) {

    client.flushdb(function (err, res) {
      User.create(testuser).then(function(res){
        userId = res.id;
        done();
      });
    });
  });

  it('should return a token which is valid for x days', function (done) {

    var login = {};
    login.username = testuser.username;
    login.password = testuser.password;

    request(app)
      .post('/login')
      .send(login)
      .expect(200)
      .end(function(err, res){
        if (err) {
          return done(err);
        }

        assert.equal(res.body.user.id, userId);
        done();
      });
  })

  it('should return a unauthorized (401) when given a wrong username', function (done) {

    var login = {};
    login.username = "wrong username";
    login.password = testuser.password;

    request(app)
      .post('/login')
      .send(login)
      .expect(401)
      .end(function(err, res){
        if (err) {
          return done(err);
        }

        done();
      });
  });

  it('should return a unauthorized (401) when given a wrong password', function (done) {

    var login = {};
    login.username = testuser.username;
    login.password = "wrong password";

    request(app)
      .post('/login')
      .send(login)
      .expect(401)
      .end(function(err, res){
        if (err) {
          return done(err);
        }

        done();
      });
  });
})
