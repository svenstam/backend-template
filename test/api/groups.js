var chai = require("chai"),
  expect = chai.expect,
  request = require('supertest'),
  client = require('../../lib/redisclient'),
  User = require('../../model/user.js'),
  Role = require('../../model/role.js'),
  Group = require('../../model/group.js'),
  app = require('../../server.js');

chai.use(require('chai-things'));
chai.should();

describe('API Groups', function () {

  var testuser = new User();
  testuser.username = "loginuser";
  testuser.password = "password";
  testuser.name = "Login User";
  testuser.role = Role.USER;

  var group1 = {name: "group1"};
  var group2 = {name: "group2"};

  var userId = null;
  var token = null;

  before(function (done) {

    client.flushdb(function (err, res) {
      User.create(testuser).then(function (res) {
        userId = res.id;

        request(app)
          .post('/login')
          .send(testuser)
          .expect(200)
          .end(function (err, res) {
            if (err) {
              return done(err);
            }

            userId = res.body.user.id;
            token = res.body.token;

            done();
          });
      });
    });
  });

  describe('POST /api/groups', function () {

    it("should create a group", function (done) {
      request(app)
        .post('/api/groups')
        .send(group1)
        .expect(200)
        .set('Authorization', "Bearer " + token)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }

          var newGroup = res.body;
          group1.id = newGroup.id;
          expect(newGroup).to.deep.equal(group1);

          done();
        });
    });

    it("should create another group", function (done) {
      request(app)
        .post('/api/groups')
        .send(group2)
        .expect(200)
        .set('Authorization', "Bearer " + token)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }

          var newGroup = res.body;
          group2.id = newGroup.id;
          expect(newGroup).to.deep.equal(group2);

          done();
        });
    });
  });

  describe('GET /api/groups', function () {

    it("should return all the created groups", function (done) {
      request(app)
        .get('/api/groups')
        .expect(200)
        .set('Authorization', "Bearer " + token)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }

          var groups = res.body;
          groups.should.include(group1, group2);
          done();
        });
    });
  });

  describe('PUT /api/groups/:id', function () {

    it("should update the group name", function (done) {

      var name = "New group name";
      request(app)
        .put('/api/groups/' + group2.id)
        .send({name: name})
        .expect(200)
        .set('Authorization', "Bearer " + token)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }

          expect(res.body.name).to.be.equal(name);
          done();
        });
    });
  });

  describe('DELETE /api/groups/:id', function () {

    it("should delete existing group", function (done) {
      request(app)
        .delete('/api/groups/' + group2.id)
        .expect(200)
        .set('Authorization', "Bearer " + token)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }

          expect(res.body).to.be.true;
          done();
        });
    });

    it("should return only group1", function (done) {
      request(app)
        .get('/api/groups')
        .expect(200)
        .set('Authorization', "Bearer " + token)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }

          var groups = res.body;
          groups.should.include(group1);
          groups.should.not.include(group2);

          done();
        });
    });

    it("should not delete a non existing group", function (done) {
      request(app)
        .delete('/api/groups/' + 3)
        .expect(400)
        .set('Authorization', "Bearer " + token)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }

          expect(res.body.status).to.be.equal(400)
          done();
        });
    });
  });

  describe('GET /api/groups/:id/members', function () {
    it("should return an empty list of existing group", function (done) {
      request(app)
        .get('/api/groups/' + group1.id + '/members')
        .expect(200)
        .set('Authorization', "Bearer " + token)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }

          expect(res.body).to.be.empty;
          done();
        });
    });

    it("should return an error of a non existing group", function (done) {
      request(app)
        .get('/api/groups/1/members')
        .expect(400)
        .set('Authorization', "Bearer " + token)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }

          done();
        });
    })
  })

  describe('POST /api/groups/:id/members/:memberid', function () {

    it("should add an existing account", function (done) {
      request(app)
        .post('/api/groups/' + group1.id + '/members/' + userId)
        .expect(200)
        .set('Authorization', "Bearer " + token)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }

          expect(res.body).to.be.true;
          done();
        });
    });

    it("should not add a non-existing account", function (done) {
      request(app)
        .post('/api/groups/' + group1.id + '/members/1')
        .expect(400)
        .set('Authorization', "Bearer " + token)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }
          var expected = {message: "MemberId not found", status: 400};

          expect(res.body).to.deep.equal(expected);
          done();
        });
    });

    it("should return an list of one member", function (done) {
      request(app)
        .get('/api/groups/' + group1.id + '/members')
        .expect(200)
        .set('Authorization', "Bearer " + token)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }

          res.body.should.include(userId);
          done();
        });
    });
  })

  describe('DELETE /api/groups/:id/members/:memberid', function () {

    it("should remove an existing account", function (done) {
      request(app)
        .delete('/api/groups/' + group1.id + '/members/' + userId)
        .expect(200)
        .set('Authorization', "Bearer " + token)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }

          expect(res.body).to.be.true;
          done();
        });
    });

    it("should return an empty list", function (done) {
      request(app)
        .get('/api/groups/' + group1.id + '/members')
        .expect(200)
        .set('Authorization', "Bearer " + token)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }

          expect(res.body).to.be.empty;
          done();
        });
    });
  })
});
