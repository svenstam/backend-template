var chai = require("chai"),
  assert = chai.assert,
  expect = chai.expect,
  request = require('supertest'),
  client = require('../../lib/redisclient'),
  User = require('../../model/user.js'),
  Role = require('../../model/role.js'),
  app = require('../../server.js');

describe('API Accounts', function () {

  var testuser = new User();
  testuser.username = "loginuser";
  testuser.password = "password";
  testuser.name = "Login User";
  testuser.role = Role.USER;

  var user1 = new User();
  user1.username = "testuser1";
  user1.password = "password";
  user1.name = "Test User1";
  user1.role = Role.USER;

  var user2 = new User();
  user2.username = "testuser2";
  user2.password = "password";
  user2.name = "Test User2";
  user2.role = Role.USER;

  var userId = null;
  var token = null;

  before(function (done) {

    client.flushdb(function (err, res) {
      User.create(testuser).then(function (res) {
        userId = res.id;

        request(app)
          .post('/login')
          .send(testuser)
          .expect(200)
          .end(function (err, res) {
            if (err) {
              return done(err);
            }

            userId = res.body.user.id;
            token = res.body.token;

            done();
          });
      });
    });
  });

  describe('POST /api/accounts', function () {

    it("should create a user", function (done) {
      request(app)
        .post('/api/accounts')
        .send(user1)
        .expect(200)
        .set('Authorization', "Bearer " + token)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }

          var newUser = new User(res.body);
          user1.id = newUser.id;
          expect(newUser).to.deep.equal(user1);

          done();
        });
    });

    it("should create a user", function (done) {
      request(app)
        .post('/api/accounts')
        .send(user2)
        .expect(200)
        .set('Authorization', "Bearer " + token)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }

          var newUser = new User(res.body);
          user2.id = newUser.id;
          expect(newUser).to.deep.equal(user2);

          done();
        });
    });

    it("should return a conflict (409)", function (done) {
      request(app)
        .post('/api/accounts')
        .send(user1)
        .expect(409)
        .set('Authorization', "Bearer " + token)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }

          assert.equal(409, res.body.status);

          done();
        });
    });
  });

  describe('GET /api/accounts', function () {

    it("should return all the created users", function (done) {

      request(app)
        .get('/api/accounts')
        .expect(200)
        .set('Authorization', "Bearer " + token)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }

          expect(res.body).to.have.any.keys('testuser1', 'testuser2');
          done();
        });
    });
  });

  describe('PUT /api/accounts/:id', function () {

    it("should update the name", function (done) {

      var name = "New name test user";

      request(app)
        .put('/api/accounts/' + user1.id)
        .send({name: name})
        .expect(200)
        .set('Authorization', "Bearer " + token)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }

          expect(res.body.name).to.be.equal(name);
          done();
        });
    });

    it("should not update the user name", function (done) {

      var username = "testuser3";
      request(app)
        .put('/api/accounts/' + user1.id)
        .send({username: username})
        .expect(200)
        .set('Authorization', "Bearer " + token)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }

          expect(res.body.username).not.to.be.equal(username);
          done();
        });
    });
  });

  describe('DELETE /api/accounts/:id', function () {

    it("should delete existing user", function (done) {
      request(app)
        .delete('/api/accounts/' + user2.id)
        .expect(200)
        .set('Authorization', "Bearer " + token)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }

          expect(res.body).to.be.true;
          done();
        });
    });

    it("should return only testuser1", function (done) {
      request(app)
        .get('/api/accounts')
        .expect(200)
        .set('Authorization', "Bearer " + token)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }

          expect(res.body).not.to.have.any.keys('testuser2');
          done();
        });
    });

    it("should not delete a non existing user", function (done) {
      request(app)
        .delete('/api/accounts/' + 3)
        .expect(400)
        .set('Authorization', "Bearer " + token)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }

          expect(res.body.status).to.be.equal(400)
          done();
        });
    });

  });
});
