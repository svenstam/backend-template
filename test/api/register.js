var chai = require("chai"),
  assert = chai.assert,
  request = require('supertest'),
  client = require('../../lib/redisclient'),
  app = require('../../server.js');

describe('POST /register', function () {

  beforeEach(function (done) {
    client.flushdb(function (err, res) {
      done();
    });
  });

  it('should return new user json', function(done){
    var user = {
      username: 'testuser1',
      password: 'password',
      name: 'TestUser1'
    };

    request(app)
      .post('/register')
      .send(user)
      .expect(200)
      .end(function(err, res){
        if (err) {
          return done(err);
        }
        assert.equal(user.username, res.body.username);
        assert.equal(user.password, res.body.password);
        assert.equal(user.name, res.body.name);
        done();
      });
  });

  it('should return a 409 on creating a duplicate user', function(done){
    var user = {
      username: 'testuser1',
      password: 'password',
      name: 'TestUser1'
    };

    request(app)
      .post('/register')
      .send(user)
      .expect(200)
      .end(function(err, res){
        if (err) {
          return done(err);
        }
        assert.equal(user.username, res.body.username);
        assert.equal(user.password, res.body.password);
        assert.equal(user.name, res.body.name);

        request(app)
          .post('/register')
          .send(user)
          .expect(409)
          .end(function(err, res){
            if (err) {
              return done(err);
            }
            done();
          });
      });
  });
});