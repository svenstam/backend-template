var chai = require("chai"),
  assert = chai.assert,
  expect = chai.expect,
  client = require('../../lib/redisclient'),
  User = require('../../model/user.js'),
  Address = require('../../model/address.js'),
  AddressType = require('../../model/addresstype.js'),
  Role = require('../../model/role.js');

chai.should();
chai.use(require('chai-things'));
describe('Address', function () {

  beforeEach(function (done) {
    client.flushdb(function (err, res) {
      done();
    });
  })
  var user = new User();
  user.username = "testuser1";
  user.password = "password";
  user.name = "Test User";
  user.role = Role.USER;

  describe("#findAddress()", function(){

    it("should find user id based on address", function(){

      return User.create(user).then(function (newUser1) {
        assert.equal(user, newUser1);
        return user.addAddress(AddressType.PHONE, "+31612345678");
      }).then(function (res) {
        assert.isTrue(res);
        return Address.findAddress("+31612345678");
      }).then(function(ids){
        ids.should.include(user.id);
      });
    });
  });
});