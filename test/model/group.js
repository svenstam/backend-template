var chai = require("chai"),
  assert = chai.assert,
  expect = chai.expect,
  client = require('../../lib/redisclient'),
  Group = require('../../model/group.js'),
  User = require('../../model/user.js'),
  Role = require('../../model/role.js'),
  should = chai.should();

chai.use(require('chai-things'));

describe('Group', function() {

  before(function (done) {
    client.flushdb(function (err, res) {
      User.create(user1).then(function(res){
        user1.id = res.id;
        return User.create(user2);
      }).then(function(res) {
        user2.id = res.id;
        done();
      });;
    });
  })

  var user1 = new User();
  user1.username = "testuser1";
  user1.password = "password";
  user1.name = "Test User";
  user1.role = Role.USER;

  var user2 = new User();
  user2.username = "testuser2";
  user2.password = "password";
  user2.name = "Test User";
  user2.role = Role.USER;

  var groupName1 = "group1";
  var groupName2 = "group2";

  var group1 = null;
  var group2 = null;

  describe("#create()", function () {

    it("should create a new group", function(){

      return Group.create(groupName1).then(function(newGroup) {

        group1 = newGroup;
        expect(newGroup.name).to.be.equal(groupName1);
      });
    });

    it("should create a another new group", function(){

      return Group.create(groupName2).then(function(newGroup) {

        group2 = newGroup;
        expect(newGroup.name).to.be.equal(groupName2);
      });
    });
  });

  describe("#exists()", function(){
    it("should say exists is true", function(){

      return Group.exists(group1.id).then(function(exists){
        assert.equal(1, exists);
      });
    });

    it("should say exists is false", function() {

      return Group.exists(1).then(function (exists) {
        assert.equal(0, exists);
      });
    });
  });

  describe("#findAll()", function() {

    it("should find 2 groups", function () {
      return Group.findAll().then(function(groups){

        groups.should.include(group1, group2);
      });
    });
  });

  describe("#delete()", function() {

    it("should remove a group without errors", function () {
      return group2.delete().then(function(deleted){
        expect(deleted).to.be.true;
      })
    })

    it("should find only one group", function () {
      return Group.findAll().then(function(groups){

        groups.should.include(group1);
        groups.should.not.include(group2);
      });
    })
  });

  describe("#save()", function(){

    it("should update name of the created group", function(){
      var newName = "New group name";

      group1.name = newName;
      return group1.save().then(function(res){
        expect(res).to.be.true

        return Group.getGroup(group1.id)
      }).then(function(foundGroup){
        expect(foundGroup.name).to.be.equal(newName);
      })
    });
  });

  describe("#addMember()", function(){

    it("should add member", function(){
      return group1.addMember(user1.id).then(function(res){
        expect(res).to.be.true;
      });
    });

    it("should not add not existing member", function(){
      return group1.addMember(1).then(function(res){
        expect(res).to.be.false;
      });
    });
  });

  describe("#getMembers()", function(){

    it("should find added member", function(){
      return group1.getMembers().then(function(members){
        members.should.contain(user1.id);
        members.should.not.contain(1);
      });
    });
  });

  describe("#removeMember()", function() {

    it("should not remove no existing member", function(){
      return group1.removeMember(1).then(function(res){
        expect(res).to.be.false;
      });
    });

    it("should remove member", function(){
      return group1.removeMember(user1.id).then(function(res) {
        expect(res).to.be.true;
      });
    });

    it("all members should be removed", function(){
      return group1.getMembers().then(function(members){
        expect(members).to.be.empty;
      });
    });
  });

  describe("#removeAllMembers()", function() {

    it("should remove all the existing members", function(){
      return group1.addMember(user1.id).then(function(res) {
        expect(res).to.true;
        return group1.addMember(user2.id);
      }).then(function(res) {
        expect(res).to.true;
        return group1.removeAllMembers(user1.id);
      }).then(function (result) {

        return group1.getMembers();
      }).then(function(members) {
        expect(members).to.be.empty;
      })
    });
  });
});