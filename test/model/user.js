var chai = require("chai"),
    assert = chai.assert,
    expect = chai.expect,
    client = require('../../lib/redisclient'),
    User = require('../../model/user.js'),
    AddressType = require('../../model/addresstype.js'),
    Role = require('../../model/role.js');

chai.should();
chai.use(require('chai-things'));

describe('User', function() {

  beforeEach(function (done) {
    client.flushdb(function (err, res) {
      done();
    });
  })

  var user1 = new User();
  user1.username = "testuser1";
  user1.password = "password";
  user1.name = "Test User";
  user1.role = Role.USER;

  var user2 = new User();
  user2.username = "testuser2";
  user2.password = "password";
  user2.name = "Test User";
  user2.role = Role.ADMIN;

  describe("#create()", function(){

    it("should create a new user", function(){

      return User.create(user1).then(function(newUser) {
        assert.equal(user1, newUser);
      });
    });

    it("should not create second new user", function(){

      return User.create(user1).then(function(newUser) {
        assert.equal(user1, newUser);
        return User.create(user1);
      }).then(function(newUser) {
        assert.isUndefined(newUser);
      });
    });
  });

  describe("#exists()", function(){
    it("should say exists is true", function(){

      return User.create(user1).then(function(newUser) {
        assert.equal(user1, newUser);
        return User.exists(user1.username);
      }).then(function(exists){
        assert.equal(1, exists);
      });
    });

    it("should say exists is false", function() {

      return User.create(user1).then(function (newUser) {
        assert.equal(user1, newUser);
        return User.exists("testuser");
      }).then(function (exists) {
        assert.equal(0, exists);
      });
    });
  });

  describe("#findAll()", function() {

    it("should find 1 user", function() {
      return User.create(user1).then(function(newUser) {
        assert.equal(user1, newUser);
        return User.findAll();
      }).then(function(users){
        expect(users).to.include.keys(user1.username);
        assert.equal(users[user1.username], user1.id);
      });
    });

    it("should find 2 users", function() {
      return User.create(user1).then(function(newUser1) {
        assert.equal(user1, newUser1);

        return User.create(user2);
      }).then(function(newUser2){
        return User.findAll();
      }).then(function(users){

        expect(users).to.include.keys(user1.username, user2.username);
        assert.equal(users[user1.username], user1.id);
        assert.equal(users[user2.username], user2.id);
      });
    });
  });

  describe("#save()", function(){

    it("should update name of the created user", function(){
      var newName = "New name test user";
      return User.create(user1).then(function(newUser) {
        assert.equal(user1, newUser);
        newUser.name = newName;
        return newUser.save()
      }).then(function(res){
        assert.isTrue(res);

        return User.getUser(user1.id)
      }).then(function(foundUser){
        assert.equal(newName, foundUser.name);
      });
    });
  });

  describe("#delete()", function(){

    it("should delete user and it's properties", function(){

      return User.create(user1).then(function(newUser) {
        assert.equal(user1, newUser);
        return newUser.delete();
      }).then(function(res){
        assert.isTrue(res);
        return User.getUserId(user1.username)
      }).then(function(id){
        assert.isNull(id);
      });
    });

    // TODO: Also test if addresses are deleted
  });

  describe("#addAddress()", function() {
    it("should add address without error", function () {
      return User.create(user1).then(function (newUser1) {
        assert.equal(user1, newUser1);
        return user1.addAddress(AddressType.PHONE, "0612345678");
      }).then(function (res) {
        assert.isTrue(res);
      });
    });

    // TODO: Test if address is found
  });

  describe("#getAddresses()", function() {
    it("should return added address", function () {

      return User.create(user1).then(function (newUser1) {
        assert.equal(user1, newUser1);

        return user1.addAddress(AddressType.PHONE, "0612345678");
      }).then(function (res) {
        assert.isTrue(res);
        return user1.getAddresses();
      }).then(function(addresses) {
        var expected = {type: AddressType.PHONE, value: "0612345678"};
        addresses.should.include(expected);
      });
    });
  });

  describe("#removeAddress()", function() {
    it("should remove address", function () {

      var address = "0612345678";
      return User.create(user1).then(function (newUser1) {
        assert.equal(user1, newUser1);
        return user1.addAddress(AddressType.PHONE, address);
      }).then(function (res) {
        assert.isTrue(res);

        return user1.removeAddress(address);
      }).then(function(res){

        assert.equal(1, res);
        return user1.getAddresses()
      }).then(function(addresses){
        assert.equal(0, addresses.length);
      });

      // TODO: Test if address is not found
    });

    describe("#removeAddresses()", function() {
      it("should remove all addresser", function () {

        var address1 = "0612345678";
        var address2 = "email@test.com";
        return User.create(user1).then(function (newUser1) {
          assert.equal(user1, newUser1);
          return user1.addAddress(AddressType.PHONE, address1);
        }).then(function (res) {
          assert.isTrue(res);

          return user1.addAddress(AddressType.EMAIL, address2);
        }).then(function (res) {
          assert.isTrue(res);

          return user1.removeAddresses();
        }).then(function (res) {

          assert.equal(2, res);
          return user1.getAddresses();
        }).then(function (addresses) {
          assert.equal(0, addresses.length);
        });
      });

      // TODO: Test if addresses is not found
    });
  });
});