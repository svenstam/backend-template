var chai = require("chai"),
  expect = chai.expect,
  client = require('../../lib/redisclient'),
  Config = require('../../model/config.js');

chai.should();
chai.use(require('chai-things'));
describe('Config', function () {

  beforeEach(function (done) {
    client.flushdb(function (err, res) {
      done();
    });
  })

  var config = null;

  describe("#load()", function () {

    it("should load the config from db with empty values", function(){

      return Config.load().then(function(cnf) {

        config = cnf;
        expect(config.askfastAccountId).to.be.null;
        expect(config.askfastKey).to.be.null;
      })
    });
  })

  describe("#save()", function () {

    it("should update config value", function(){

      var askfastAccountId = "1";
      var askfastKey = "2";

      config.askfastAccountId = askfastAccountId;
      config.askfastKey = askfastKey;
      return config.save().then(function(saved){
        expect(saved).to.be.true
        return Config.load();
      }).then(function(cnf) {

        config = cnf;
        expect(config.askfastAccountId).to.be.equal(askfastAccountId);
        expect(config.askfastKey).to.be.equal(askfastKey);
      })
    });
  })
})